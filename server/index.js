const nodemailer = require('nodemailer');
const express = require('express');
const cors = require('cors');
const path = require('path');
const bodyParser = require('body-parser');
const moment = require('moment');
require('moment/locale/ru');

const clientTypes = {
	1: 'Advertising agency',
	2: 'Direct Advertiser',
	3: 'Publisher',
	4: 'Freelance',
	5: 'Other',
};

require('dotenv').config({ path: path.resolve(process.cwd(), `../.env.${process.env.NODE_ENV}`) });

const app = express();
app.use(cors());
app.use(bodyParser.json());

const tConf = {
	auth: {
		user: process.env.MAIL_USERNAME,
		pass: process.env.MAIL_PASSWORD,
	},
};
console.log(process.env.MAIL_USERNAME, process.env.MAIL_PASSWORD);
if (process.env.NODE_ENV === 'development') {
	tConf.service = 'gmail';
}
if (process.env.NODE_ENV === 'production') {
	tConf.host = process.env.MAIL_HOST;
	tConf.port = process.env.MAIL_PORT;
}

const transporter = nodemailer.createTransport(tConf);
const mailOptions = {
	from: process.env.MAIL_FROM,
	to: process.env.MAIL_TO,
};

const submit = (options, callback) => {
	transporter.sendMail(options, function(error, info) {
		if (error) {
			console.error(error);
			throw new Error(error);
		} else {
			callback();
		}
	});
};
/*
app.post('/', function(req, res) {
	const {first_name, second_name, email, tg, datetime, locale, type, company, client_type } = req.body;
	const date = new Date(datetime);
	const dateFormatted = moment(date).format();
	let subject = 'From Ubex landing: ';
	if(type === 'tour') {
		subject += 'Desk Tour new order'
	}
	if(type === 'partner') {
		subject += 'Become a partner'
	}
	const options = {
		...mailOptions,
		subject,
		text: `
			Имя: ${first_name} ${second_name}
			Email: ${email}
			${tg ? `Telegram / Skype: ${tg}` : '' }
			${type === 'tour' ? `Время: ${dateFormatted}` : '' }
			${type === 'partner' ? `Компания: ${company}` : '' }
			Тип клиента: ${clientTypes[client_type]}
			Язык: ${locale}
		`
	};
	console.log(options);
	submit(options, res.send(req.body));
});
*/
app.post('/forms', function(req, res) {
	const { name, company, phone, comment, type, messenger, locale } = req.body;
	let subject = 'From Ubex landing: ';
	if (type === 'message') {
		subject += 'Message';
	}
	if (type === 'call') {
		subject += 'Callback';
	}
	const options = {
		...mailOptions,
		subject,
		text: `
			Имя: ${name}
			Телефон: ${phone}
			${messenger ? `Мессенджер: ${messenger}` : ''}
			${company ? `Компания: ${company}` : ''}
			${company ? `Комментарий: ${comment}` : ''}
			Язык: ${locale}
		`,
	};
	submit(options, res.send(req.body));
});

const port = process.env.APP_PORT || 3000;
app.listen(port, function() {
	console.log(`App listening on port ${port}, in ${process.env.NODE_ENV} env`);
});
