import { useEffect } from 'react';

const useScript = (url, tag = 'body') => {
	useEffect(() => {
		const script = document.createElement('script');

		script.src = url;
		script.async = true;

		document[tag].appendChild(script);

		return () => {
			document[tag].removeChild(script);
		};
	}, [url]);
};

export default useScript;
