import React, { useState } from 'react';
import { Form as FinalForm, Field } from 'react-final-form';
import { FormattedMessage, FormattedHTMLMessage, injectIntl } from 'react-intl';
import * as yup from 'yup';
import Datetime from 'react-datetime';
import moment from 'moment';
import yupLocales from '../../yupLocales';
import FormField from '../FormField';
import Button from '../Button';
import { makeValidate, makeRequired } from 'utils/validation';
import clientsTypes from 'clients-types';

const clientTypesIds = Object.keys(clientsTypes);
const OrderTourForm = ({ onSubmit, intl }) => {
	const [isLoading, setFormSubmitRequest] = useState(false);
	const [submitSuccess, setFormSubmitSuccess] = useState(false);
	const [submitFailed, setFormSubmitFailed] = useState(null);
	const [dateTime, setDateTime] = useState(moment());
	yup.setLocale(yupLocales[intl.locale]);
	const validationSchema = yup.object().shape({
		first_name: yup
			.string()
			.required()
			.label(intl.formatMessage({ id: 'OrderTourForm.firstName' })),
		second_name: yup
			.string()
			.required()
			.label(intl.formatMessage({ id: 'OrderTourForm.secondName' })),
		email: yup
			.string()
			.email()
			.required()
			.label(intl.formatMessage({ id: 'OrderTourForm.email' })),
		tg: yup.string().label(intl.formatMessage({ id: 'OrderTourForm.tg' })),
		client_type: yup
			.string() //.test({ test: value => clientTypesIds.indexOf(value.toString()) >= 0})
			.label(intl.formatMessage({ id: 'OrderTourForm.client_type' })),
		datetime: yup
			.date()
			.required()
			.test('is-greater', intl.formatMessage({ id: 'validation.date.min' }), value =>
				moment(value).isSameOrAfter(moment()),
			)
			.label(intl.formatMessage({ id: 'OrderTourForm.datetime' })),
	});

	const required = makeRequired(validationSchema);
	const validate = makeValidate(validationSchema);
	if (submitSuccess) {
		return (
			<h1>
				<FormattedMessage id="OrderTourForm.submitSuccess" />
			</h1>
		);
	}
	return (
		<FinalForm
			initialValues={{
				client_type: clientTypesIds[0],
			}}
			onSubmit={values => {
				setFormSubmitRequest(true);
				setFormSubmitSuccess(false);
				setFormSubmitFailed(null);
				onSubmit(values)
					.then(res => {
						setFormSubmitRequest(false);
						setFormSubmitSuccess(true);
					})
					.catch(e => {
						setFormSubmitRequest(false);
						setFormSubmitFailed(e);
					});
			}}
			validate={validate}
		>
			{({ handleSubmit }) => (
				<form onSubmit={handleSubmit}>
					{submitFailed && (
						<h1 style={{ color: 'red' }}>
							<FormattedMessage id="OrderTourForm.submitFailed" />
						</h1>
					)}
					<FormField
						required={required.first_name}
						name="first_name"
						label={<FormattedMessage id="OrderTourForm.firstName" />}
					/>
					<FormField
						required={required.second_name}
						name="second_name"
						label={<FormattedMessage id="OrderTourForm.secondName" />}
					/>
					<FormField
						required={required.email}
						name="email"
						type={'email'}
						label={<FormattedMessage id="OrderTourForm.email" />}
					/>
					<FormField required={required.email} name="tg" label={<FormattedMessage id="OrderTourForm.tg" />} />
					<Field name="datetime" required={required.datetime}>
						{({ input, meta }) => (
							<div className="form-group">
								<label htmlFor={`${name}_input`}>
									<FormattedMessage id="OrderTourForm.datetime" />
								</label>
								<Datetime
									id={`${name}_input`}
									className="form-control"
									{...input}
									defaultValue={moment()}
									locale={intl.locale}
									onChange={e => {
										setDateTime(e);
										input.onChange(e);
									}}
									timeConstraints={getValidTimes(dateTime)}
									isValidDate={current => current.isSameOrAfter(Datetime.moment())}
								/>
								{meta.touched && meta.error && <div className="invalid-feedback">{meta.error}</div>}
							</div>
						)}
					</Field>
					<Field name="client_type" required={required.client_type}>
						{({ input, meta }) => (
							<div className="form-group">
								<label htmlFor={`client_type_input`}>
									<FormattedMessage id="OrderTourForm.client_type" />
								</label>
								<select id={`client_type_input`} className="form-control" {...input}>
									{clientTypesIds.map(cid => (
										<FormattedMessage id={`OrderTourForm.client_type_${cid}`} key={cid}>
											{msg => <option value={cid}>{msg}</option>}
										</FormattedMessage>
									))}
								</select>
								{meta.touched && meta.error && <div className="invalid-feedback">{meta.error}</div>}
							</div>
						)}
					</Field>
					<Button as="button" type="submit" disabled={isLoading}>
						{!isLoading ? (
							<FormattedMessage id="OrderTourForm.btn" />
						) : (
							<FormattedMessage id="OrderTourForm.submitRequest" />
						)}
					</Button>
				</form>
			)}
		</FinalForm>
	);
};

export default injectIntl(OrderTourForm);

const getValidTimes = function(dateTime) {
	// date is today, so only allow future times
	if (moment().isSame(dateTime, 'day')) {
		return {
			hours: {
				min: dateTime.hours() >= 9 ? dateTime.hours() : 9,
				max: 18,
				step: 1,
			},
			minutes: {
				min: dateTime.minutes(),
				max: 59,
				step: 5,
			},
		};
	}

	// date is in the future, so allow all times
	return {
		hours: {
			min: 9,
			max: 18,
			step: 1,
		},
		minutes: {
			min: 0,
			max: 59,
			step: 5,
		},
	};
};
