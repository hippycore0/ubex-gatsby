import React, { useState } from 'react';
import { FormattedMessage, FormattedHTMLMessage, injectIntl } from 'react-intl';
import axios from 'axios';
import https from 'https';
import Notice from '../Notice';
import Popup from '../PopUp';
import Button from '../Button';
import OrderTourForm from './OrderTourForm';
import ymReachGoal from 'utils/ymReachGoal';
import gaReachGoal from 'utils/gaReachGoal';

const TourNotice = ({ isOpen, onClickBtn, onClickClose }) => (
	<Notice isOpen={isOpen} onClickClose={onClickClose} delay={20}>
		<h1>
			<FormattedMessage id="TourNotice.header" />
		</h1>
		<p>
			<FormattedHTMLMessage id="TourNotice.text" />
		</p>
		<Button onClick={_ => onClickBtn()}>
			<FormattedMessage id="TourNotice.btn" />
		</Button>
	</Notice>
);

const port = 3001;
const host = process.env.APP_HOST || 'http://localhost';
const agent = new https.Agent({
	rejectUnauthorized: false,
});

const TourPopUp = ({ isOpen, onClickClose, locale }) => {
	return (
		<Popup isOpen={isOpen} onClickClose={onClickClose} width={'auto'}>
			<OrderTourForm
				onSubmit={values => {
					ymReachGoal('order_tour', values);
					gaReachGoal('form', 'order_tour');
					return axios.post(
						`${host}:${port}`,
						{ ...values, type: 'tour', locale: locale },
						{ httpsAgent: agent },
					);
				}}
			/>
		</Popup>
	);
};

const TourLayout = ({ intl: { locale } }) => {
	const [noticeIsOpen, toggleNotice] = useState(true);
	const [popUpIsOpen, togglePopUp] = useState(false);
	return (
		<>
			<TourNotice
				isOpen={noticeIsOpen}
				onClickClose={_ => toggleNotice(false)}
				onClickBtn={_ => {
					togglePopUp(true);
					toggleNotice(false);
				}}
			/>
			<TourPopUp isOpen={popUpIsOpen} locale={locale} onClickClose={_ => togglePopUp(false)} />
		</>
	);
};

export default injectIntl(TourLayout);
