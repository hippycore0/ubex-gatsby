import React from 'react';
import PropTypes from 'prop-types';
import { injectIntl } from 'react-intl';
import useScript from 'utils/useScript';

const JivoSite = ({ id, intl: { locale } }) => {
	if (locale !== 'ru') {
		return null;
	}
	useScript(`//code-ya.jivosite.com/widget/${id}`, 'head');
	if (typeof window !== 'undefined') {
		window.jivo_onLoadCallback = () => {
			const { jivo_api } = window;
			if (!jivo_api) {
				return;
			}
		};
	}
	return null;
};

JivoSite.defaultProps = {
	intl: {
		locale: 'en',
	},
};

JivoSite.propTypes = {
	id: PropTypes.string.isRequired,
	intl: PropTypes.object,
};

export default injectIntl(JivoSite);
