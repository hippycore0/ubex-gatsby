import React, { useState } from 'react';
import styled, { keyframes } from 'styled-components';
import colors from 'utils/colors';
import CloseBtn from 'components/CloseBtn';
import { light as gradientLight } from 'utils/gradients';

const fadeIn = keyframes`
  0% {
    opacity: 0;
  }
  75% {
	opacity: 0;
  }
  100% {
    opacity: 1;
  }
`;

export const NoticeBody = styled.div`
	position: fixed;
	left: 10px;
	bottom: 10px;
	z-index: 90;
	padding: 1rem;
	box-shadow: 0 5px 10px rgba(0, 0, 0, 0.3);
	border-radius: 8px;
	animation: ${props => props.delay}s ${fadeIn} ease-out;
	${gradientLight}
	color: ${colors.dark};
	h1 {
		font-weight: bold;
		font-size: 1.25em;
		margin-bottom: 1em;
	}
`;

const Notice = ({ children, isOpen, onClickClose, delay = 10 }) => {
	if (!isOpen) {
		return null;
	}
	return (
		<NoticeBody delay={delay}>
			<CloseBtn color={'white'} shadow onClick={() => onClickClose()}>
				&times;
			</CloseBtn>
			{children}
		</NoticeBody>
	);
};
export default Notice;
