export default {
	1: 'Advertising agency',
	2: 'Direct Advertiser',
	3: 'Publisher',
	4: 'Freelance',
	5: 'Other',
};
