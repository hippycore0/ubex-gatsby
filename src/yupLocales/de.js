export default {
	mixed: {
		required: 'Pflichtfeld',
	},
	string: {
		email: 'E-Mail Adresse eingeben',
	},
};
