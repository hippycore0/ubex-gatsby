import en from 'yup/lib/locale';
import ru from './ru';
import de from './de';

export default { en, ru, de };
