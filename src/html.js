import React from 'react';
import PropTypes from 'prop-types';

export default function HTML(props) {
	return (
		<html {...props.htmlAttributes}>
			<head>
				<meta charSet="utf-8" />
				<meta httpEquiv="x-ua-compatible" content="ie=edge" />
				<meta name="google-site-verification" content="ZVw5wmUMZ0vUX1H_p22IFtmW8GKUm2U-V4xiRGvpNCs" />
				<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
				{props.headComponents}
			</head>
			<body {...props.bodyAttributes}>
				{props.preBodyComponents}
				<div key={`body`} id="___gatsby" dangerouslySetInnerHTML={{ __html: props.body }} />
				{props.postBodyComponents}
				<img
					height="1"
					width="1"
					style={{ display: 'none' }}
					alt=""
					src="https://px.ads.linkedin.com/collect/?pid=2699769&conversionId=3126249&fmt=gif"
				/>
				<img src="//pixel.ubex.com/pixel.gif?c=AUX-00000013" style={{ display: 'none' }} alt="" />
			</body>
		</html>
	);
}

HTML.propTypes = {
	htmlAttributes: PropTypes.object,
	headComponents: PropTypes.array,
	bodyAttributes: PropTypes.object,
	preBodyComponents: PropTypes.array,
	body: PropTypes.string,
	postBodyComponents: PropTypes.array,
};
