import React from 'react';
import styled from 'styled-components';
import { FormattedMessage } from 'react-intl';
import { Row, Col } from 'react-awesome-styled-grid';
import colors from 'utils/colors';

import CreativeIcon1 from 'images/svg/Block_2/Icon_2_1.svg';
import CreativeIcon2 from 'images/svg/Block_2/Icon_2_2.svg';
import CreativeIcon3 from 'images/svg/Block_2/Icon_2_3.svg';
import CreativeIcon4 from 'images/svg/Block_2/Icon_2_4.svg';

const icons = [CreativeIcon1, CreativeIcon2, CreativeIcon3, CreativeIcon4];
const StyledIcon = styled.svg`
	width: 2.3em;
	height: 2.3em;
	margin-right: 1em;
	margin-bottom: 1em;
	vertical-align: middle;
`;
const CreativeIcon = ({ n }) => (
	<StyledIcon>
		<use xlinkHref={`#${icons[n].id}`} />
	</StyledIcon>
);

const CreativeTypeBlockStyled = styled.div`
	background-color: #fff;
	padding: 1.2em;
	margin-bottom: 1.35em;
	border-radius: 8px;
	box-shadow: 0 5px 10px rgba(100, 100, 100, 0.3);
	.value {
		font-size: 3em;
		font-weight: 300;
		color: ${colors.primary};
		margin-bottom: 0.25em;
	}
	.label {
		font-weight: bold;
	}
`;

const creativeTypes = {
	banner: 65,
	video: 25,
	native: 5,
	audio: 5,
};

const CreativeTypeBlock = ({ icon, value, label }) => (
	<CreativeTypeBlockStyled>
		<div className="icon">{icon}</div>
		<div className="value">{value}%</div>
		<div className="label">{label}</div>
	</CreativeTypeBlockStyled>
);

const CreativeTypesBlocks = () => (
	<Row align="center" justify="center">
		{Object.keys(creativeTypes).map((key, i) => (
			<Col xs={2} lg={6} key={key}>
				<FormattedMessage id={`CreativeTypes.${key}`}>
					{msg => <CreativeTypeBlock icon={<CreativeIcon n={i} />} value={creativeTypes[key]} label={msg} />}
				</FormattedMessage>
			</Col>
		))}
	</Row>
);

export default CreativeTypesBlocks;
