import React from 'react';
import PieChart from 'react-minimal-pie-chart';
import styled from 'styled-components';
import colors from 'utils/colors';
import { injectIntl } from 'react-intl';

const TrafficData = {
	ru: [
		{ title: 'Asia', value: 14, color: colors.primary },
		{ title: 'Russia', value: 41, color: colors.secondary },
		{ title: 'Europe', value: 27, color: colors.info },
		{ title: 'America', value: 18, color: colors.alt },
	],
	en: [
		{ title: 'Asia', value: 14, color: colors.primary },
		{ title: 'America', value: 41, color: colors.alt },
		{ title: 'Europe', value: 27, color: colors.info },
		{ title: 'Russia', value: 18, color: colors.secondary },
	],
	de: [
		{ title: 'Asia', value: 14, color: colors.primary },
		{ title: 'Europe', value: 41, color: colors.info },
		{ title: 'Russia', value: 18, color: colors.secondary },
		{ title: 'America', value: 27, color: colors.alt },
	],
};

const ChartLabelStyled = styled.div`
	font-size: 0.7rem;
	padding: 0.5rem 0.85rem;
	margin: 1.2rem;
	font-weight: 500;
	font-family: Roboto, sans-serif;
	background-color: #fff;
	border-radius: 0.6rem;
	box-shadow: 0 5px 10px rgba(100, 100, 100, 0.3);
	span {
		color: ${props => props.color};
	}
`;
const ChartLabel = ({ data, dataIndex, ...props }) => {
	const { title, value, color } = data[dataIndex];
	return (
		<foreignObject x={props.x + props.dx - 70} y={props.y + props.dy} width="140" height="200">
			<ChartLabelStyled color={color}>
				{title} <span>{value}%</span>
			</ChartLabelStyled>
		</foreignObject>
	);
};
const TrafficChart = ({ intl: { locale }, ...props }) => (
	<div>
		<PieChart
			style={{ maxWidth: '500px', width: '100%' }}
			data={TrafficData[locale]}
			radius={40}
			cx={50}
			cy={50}
			startAngle={220}
			lineWidth={50}
			paddingAngle={5}
			labelPosition={80}
			label={<ChartLabel />}
			viewBoxSize={[300, 300]}
			{...props}
		/>
	</div>
);

export default injectIntl(TrafficChart);
