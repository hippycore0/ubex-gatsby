import React from 'react';
import styled from 'styled-components';
import { Container, Row, Col, config } from 'react-awesome-styled-grid';
import { FormattedMessage, injectIntl } from 'react-intl';
import colors from 'utils/colors';
import Button from 'components/Button';

const GiftScreenWrapper = styled.div`
	background-color: ${colors.warning};
	color: #fff;
`;

const GiftScreenContainer = styled(Container)`
	padding-top: 3rem;
	padding-bottom: 3rem;
	background: url(/img/gift.png) no-repeat bottom 120px center;
	text-align: center;

	h3 strong {
		display: block;
		font-size: 1.4em;
	}
	${props => config(props).media['md']`
		background: url(/img/gift.png) no-repeat right 50px center;
		text-align: left;
		h3  {
			font-size: 1.6em;
			white-space: nowrap;
		}
		p {
			font-size: 1.6em;
		}
	`}
`;

const InfoCol = styled(Col)`
	margin-bottom: 20rem;
	${props => config(props).media['md']`
		margin-bottom: 0;
	`}
`;

const GiftScreen = () => (
	<GiftScreenWrapper>
		<GiftScreenContainer>
			<Row>
				<Col md={10} offset={{ md: 1 }}>
					<Row>
						<InfoCol xs={12} sm={12} md={4} align={{ sm: 'center', md: 'flex-start' }}>
							<h3>
								<FormattedMessage
									id="GiftScreen.header"
									values={{
										s: (...chunks) => <strong>{chunks}</strong>,
										v: '20$ - 100',
									}}
								/>
							</h3>
							<p>
								<FormattedMessage id="GiftScreen.subheader" />
							</p>
						</InfoCol>
						<Col md={4} align="center" justify="center">
							<Button
								color="primary"
								href="https://passport.ubex.com/registration/?next=https://desk.ubex.com/accounts/login/?next=%2F"
								target="_blank"
							>
								<FormattedMessage id="GiftScreen.actionBtn" />
							</Button>
						</Col>
					</Row>
				</Col>
			</Row>
		</GiftScreenContainer>
	</GiftScreenWrapper>
);

export default GiftScreen;
