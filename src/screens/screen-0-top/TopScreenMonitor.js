import styled, { keyframes } from 'styled-components';
import { config } from 'react-awesome-styled-grid';
import colors from '../../utils/colors';
import React from 'react';
import PlayIcon from '../../images/svg/Header/play-icon.svg';
const move = keyframes`
    0%{background-position:7% 0%}
    50%{background-position:94% 100%}
    100%{background-position:7% 0%}
`;

const AnimatedGradient = styled.div`
	width: 1em;
	height: 1em;
	font-size: 30px;
	background: linear-gradient(323deg, ${colors.primary}, ${colors.secondary});
	background-size: 400% 400%;
	border-radius: 100%;
	display: flex;
	align-items: center;
	justify-content: center;
	${props => config(props).media['md']`
		font-size: 72px;
	 `}
	${props => config(props).media['lg']`
		font-size: 88px;
	 `}
`;

const TopScreenMonitor = styled.div`
	background: url(/img/screen.png) no-repeat center;
	background-size: contain;
	width: 100%;
	height: 174px;
	margin-top: 3rem;
	cursor: pointer;
	position: relative;
	display: flex;
	align-items: center;
	justify-content: center;
	&:hover ${AnimatedGradient} {
		animation: ${move} 2s ease infinite;
		background-position: 94% 100%;
	}
	padding-top: 12px;
	padding-bottom: 64px;
	${props => config(props).media['md']`
		margin-top: 0rem;
		width: 518px;
		height: 263px;
		position: absolute;
		left: 50%;
		z-index: 2;
		top: calc(50% - 200px);
		padding-top: 20px;
		padding-bottom: 127px;
  `}
	${props => config(props).media['lg']`
		margin-top: 0rem;
		width: 647px;
		height: 346px;
		position: absolute;
		left: 50%;
		z-index: 2;
		top: calc(50% - 275px);
		padding-top: 28px;
		padding-bottom: 157px;
  `}
   @media(max-height: 650px) {
		top: calc(50% - 275px);
	}
`;

const StyledPlayIcon = styled.svg`
	width: 12px;
	height: 16px;
	margin-right: -7%;
	${props => config(props).media['md']`
		width: 24px;
		height: 33px;
	 `}
	${props => config(props).media['lg']`
		width: 30px;
		height: 40px;
	 `}
`;

const PlayIconComponent = () => (
	<StyledPlayIcon>
		<use xlinkHref={`#${PlayIcon.id}`} />
	</StyledPlayIcon>
);

export default ({ onClick }) => (
	<TopScreenMonitor onClick={onClick}>
		<AnimatedGradient>
			<PlayIconComponent />
		</AnimatedGradient>
	</TopScreenMonitor>
);
