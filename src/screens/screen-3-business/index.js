import React from 'react';
import { FormattedMessage } from 'react-intl';
import styled from 'styled-components';
import { linearGradient } from 'polished';
import { Container, Row, Col, config } from 'react-awesome-styled-grid';
import colors from 'utils/colors';

import TariffsScreen from 'screens/screen-tariffs';
import BusinessIcon1 from 'images/svg/Block_4/Icon_4_1.svg';
import BusinessIcon2 from 'images/svg/Block_4/Icon_4_2.svg';
import BusinessIcon3 from 'images/svg/Block_4/Icon_4_3.svg';
import BusinessIcon4 from 'images/svg/Block_4/Icon_4_4.svg';

const icons = [BusinessIcon1, BusinessIcon2, BusinessIcon3, BusinessIcon4];

const BusinessScreenBg = styled.div`
	overflow: hidden;
	position: relative;
	${linearGradient({
		colorStops: [
			`${colors.darkness} 0%`,
			`${colors.darkness} 35%`,
			`${colors.dark} 35%`,
			`${colors.dark} 75%`,
			`${colors.darkness} 75%`,
			`${colors.darkness} 100%`,
		],
		toDirection: '115deg',
		fallback: colors.dark,
	})}
	color: white;
`;
const BusinessScreenContainer = styled(Container)`
	position: relative;
	padding-top: 3em;
	padding-bottom: 3em;
	h3 {
		font-weight: bold;
	}
	${props => config(props).media['md']`
		padding-top: 4em;
		padding-bottom: 7em;
	`}
`;

const BusinessScreenCard = styled.div`
	background: #fff;
	color: #000;
	padding: 1.25rem;
	border-radius: 6px;
	margin-bottom: 1.5rem;
	height: 100%;
`;

const StyledIcon = styled.svg`
	width: 2.7em;
	height: 2.7em;
	margin-right: 1em;
	vertical-align: middle;
	display: block;
	margin-bottom: 2.5rem;
`;

const Icon = ({ n }) => (
	<StyledIcon>
		<use xlinkHref={`#${icons[n].id}`} />
	</StyledIcon>
);

const BusinessScreen = () => (
	<BusinessScreenBg>
		<BusinessScreenContainer>
			<Row>
				<Col md={10} offset={{ md: 1 }}>
					<h3>
						<FormattedMessage id="BusinessScreen.header" />
					</h3>
					<Row>
						<Col sm={3} md={3}>
							<BusinessScreenCard>
								<Icon n={0} />
								<span className="BusinessScreen__card-label">
									<FormattedMessage id="BusinessScreen.card1" values={{ v: '10$' }} />
								</span>
							</BusinessScreenCard>
						</Col>
						<Col sm={3} md={3}>
							<BusinessScreenCard>
								<Icon n={1} />
								<span className="BusinessScreen__card-label">
									<FormattedMessage id="BusinessScreen.card2" />
								</span>
							</BusinessScreenCard>
						</Col>
						<Col sm={3} md={3}>
							<BusinessScreenCard>
								<Icon n={2} />
								<span className="BusinessScreen__card-label">
									<FormattedMessage id="BusinessScreen.card3" />
								</span>
							</BusinessScreenCard>
						</Col>
						<Col sm={3} md={3}>
							<BusinessScreenCard>
								<Icon n={3} />
								<span className="BusinessScreen__card-label">
									<FormattedMessage id="BusinessScreen.card4" />
								</span>
							</BusinessScreenCard>
						</Col>
					</Row>
				</Col>
			</Row>
		</BusinessScreenContainer>
	</BusinessScreenBg>
);

export default BusinessScreen;
