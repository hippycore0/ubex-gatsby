import React, { useState } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { config } from 'react-awesome-styled-grid';
import { FormattedMessage, FormattedHTMLMessage } from 'react-intl';
import Button from 'components/Button';
import colors from 'utils/colors';
import ymReachGoal from '../../utils/ymReachGoal';
import gaReachGoal from '../../utils/gaReachGoal';

const StyledTariffCard = styled.div`
	background: white;
	text-align: center;
	padding: 50px 30px;
	border-radius: 15px;
	height: 100%;
	display: flex;
	flex-direction: column;
	justify-content: space-between;
	margin-bottom: 1em;
	${props => config(props).media['md']`
		margin-bottom: 0;
			${props =>
				props.isCenter &&
				`
				box-shadow: 0px 0px 15px -2px rgb(0 0 0 / 20%);
				margin-top: -20px;
				margin-bottom: -20px;
				padding-top: 70px;
				padding-bottom: 70px;
			`}
	`}
`;

const TariffHeader = styled.h3`
	margin-bottom: 0;
`;
const TariffSubHeader = styled.p``;
const TariffPrice = styled.div`
	font-size: 2.8em;
	font-weight: bold;
	margin-bottom: 0.5em;
	color: ${props => colors[props.color]};
`;
const TariffDescription = styled.div`
	font-size: 0.8em;
	margin-bottom: 3em;
	ul > li {
		margin-bottom: 1.2em;
	}
`;
const PeriodSelector = styled.select`
	display: block;
	margin-bottom: 2.5em;
	text-align-last: center;
`;

const TariffButton = styled(Button)`
	display: block;
`;

const TariffUpperPart = styled.div``;
const TariffBottomPart = styled.div``;
const tariffConf = {
	base: {
		price: 0,
		limit: 0,
	},
	optimal: {
		price: 49.99,
		limit: 1000,
	},
	business: {
		price: 149.99,
		limit: 10000,
	},
};

const TariffCard = ({ alias, usePeriod, isCenter }) => {
	const [period, setPeriod] = useState(3);
	return (
		<StyledTariffCard isCenter={isCenter}>
			<TariffUpperPart>
				<TariffHeader>
					<FormattedMessage id={`TariffCard.${alias}.header`} />
				</TariffHeader>
				<TariffSubHeader>
					<FormattedMessage
						id={`TariffCard.${alias}.subheader`}
						values={{ v: tariffConf[alias].limit.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ' ') }}
					/>
				</TariffSubHeader>
				<TariffPrice color={isCenter ? 'tariffBtn' : 'secondary'}>
					{tariffConf[alias].price === 0 ? (
						<FormattedHTMLMessage id={`TariffCard.free`} />
					) : (
						`$${tariffConf[alias].price}`
					)}
				</TariffPrice>
				<TariffDescription>
					<FormattedHTMLMessage id={`TariffCard.${alias}.description`} />
				</TariffDescription>
			</TariffUpperPart>
			<TariffBottomPart>
				{usePeriod && (
					<PeriodSelector value={period} className="form-control" onChange={e => setPeriod(e.target.value)}>
						<FormattedMessage id="TariffPeriod.1m">{m => <option value={1}>{m}</option>}</FormattedMessage>
						<FormattedMessage id="TariffPeriod.3m" values={{ sale: 5 }}>
							{m => <option value={3}>{m}</option>}
						</FormattedMessage>
						<FormattedMessage id="TariffPeriod.6m" values={{ sale: 15 }}>
							{m => <option value={6}>{m}</option>}
						</FormattedMessage>
						<FormattedMessage id="TariffPeriod.12m" values={{ sale: 35 }}>
							{m => <option value={12}>{m}</option>}
						</FormattedMessage>
					</PeriodSelector>
				)}
				<TariffButton
					color={isCenter ? 'tariffBtn' : 'secondary'}
					href={`https://passport.ubex.com/registration/?tariff=${alias}&period=${period}`}
					target="_blank"
					onClick={e => {
						ymReachGoal(alias);
						window.open(e.target.href);
					}}
				>
					<FormattedMessage id="TariffCard.button" />
				</TariffButton>
			</TariffBottomPart>
		</StyledTariffCard>
	);
};

TariffCard.propTypes = {
	alias: PropTypes.oneOf(['base', 'optimal', 'business']).isRequired,
	isCenter: PropTypes.bool,
	usePeriod: PropTypes.bool,
};

TariffCard.defaultProps = {
	isCenter: false,
	usePeriod: true,
};

export default TariffCard;
