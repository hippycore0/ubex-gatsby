import React from 'react';
import styled from 'styled-components';
import { FormattedMessage } from 'react-intl';
import { Col, Container, Row } from 'react-awesome-styled-grid';
import TariffCard from './tariffCard';
import { linearGradient } from 'polished';
import colors from '../../utils/colors';
const TariffScreenBg = styled.div`
	overflow: hidden;
	position: relative;
	${linearGradient({
		colorStops: [
			`${colors.lightness} 0%`,
			`${colors.lightness} 50%`,
			`${colors.light} 50%`,
			`${colors.light} 100%`,
		],
		toDirection: '-115deg',
		fallback: colors.lightness,
	})}
`;
const TariffScreenContainer = styled(Container)`
	padding-top: 6em;
	color: #000;
	padding-bottom: 7em;
	h3 {
		font-weight: bold;
	}
`;

const CardWrapper = styled.div`
	padding-top: 1em;
`;

const TariffsScreen = () => {
	return (
		<TariffScreenBg>
			<TariffScreenContainer>
				<Row>
					<Col md={10} offset={{ md: 1 }}>
						<h3>
							<FormattedMessage id="TariffsScreen.header" />
						</h3>
						<p>
							<FormattedMessage id="TariffsScreen.subheader" />
						</p>
						<CardWrapper>
							<Row>
								<Col sm={4}>
									<TariffCard alias="base" usePeriod={false} />
								</Col>
								<Col sm={4}>
									<TariffCard alias="optimal" isCenter />
								</Col>
								<Col sm={4}>
									<TariffCard alias="business" />
								</Col>
							</Row>
						</CardWrapper>
					</Col>
				</Row>
			</TariffScreenContainer>
		</TariffScreenBg>
	);
};

export default TariffsScreen;
