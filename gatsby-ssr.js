/**
 * Implement Gatsby's SSR (Server Side Rendering) APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/ssr-apis/
 */

const React = require('react');

exports.onRenderBody = ({ setHeadComponents }, pluginOptions) => {
	return setHeadComponents([
		<>
			<script
				key={`gatsby-plugin-facebook-pixel`}
				dangerouslySetInnerHTML={{
					__html: `
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window, document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '2943863759234215');
fbq('track', 'PageView');
      `,
				}}
			/>
			<noscript>
				<img
					height="1"
					width="1"
					style={{ display: 'none' }}
					src="https://www.facebook.com/tr?id=2943863759234215&ev=PageView&noscript=1"
				/>
			</noscript>
			<script key={'test-mentrika'}
			        dangerouslySetInnerHTML={{
				        __html: `
				        (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
   m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
   (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

   ym(85813049, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor: true,
   });
				        `}}
			/>
		</>,
	]);
};
