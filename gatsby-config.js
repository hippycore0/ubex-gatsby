require('dotenv').config({});
module.exports = {
	siteMetadata: {
		title: `Ubex - Artificial Intelligence in Advertising`,
		description: `Ubex is a global decentralized advertising exchange where companies advertise effectively, while publishers profitably tokenize ad slots on their resources.`,
		author: `@ubex-ai`,
	},
	plugins: [
		`gatsby-plugin-svg-sprite`,
		`gatsby-plugin-resolve-src`,
		`gatsby-plugin-react-helmet`,
		`gatsby-plugin-styled-components`,
		`gatsby-transformer-sharp`,
		`gatsby-plugin-sharp`,
		// `gatsby-plugin-webpack-bundle-analyser-v2`,
		{
			resolve: `gatsby-plugin-anchor-links`,
			options: {
				offset: -100,
			},
		},
		{
			resolve: `gatsby-plugin-intl`,
			options: {
				// language JSON resource path
				path: `${__dirname}/src/intl`,
				// supported language
				languages: [`en`, `ru`],
				// language file path
				defaultLanguage: `en`,
				// option to redirect to `/ko` when connecting `/`
				redirect: false,
			},
		},
		{
			resolve: `gatsby-plugin-s3`,
			options: {
				bucketName: 'landing.ubex',
				region: ' us-central-1',
				acl: null,
			},
		},
		{
			resolve: `gatsby-plugin-sass`,
			options: {
				includePaths: [
					// require("bourbon-neat").includePaths
				],
			},
		},
		{
			resolve: 'gatsby-plugin-web-font-loader',
			options: {
				google: {
					families: ['Roboto:italic,300,400,500,700,900'],
				},
			},
		},
		{
			resolve: `gatsby-source-filesystem`,
			options: {
				name: `images`,
				path: `${__dirname}/src/images`,
			},
		},
		{
			resolve: `gatsby-plugin-google-gtag`,
			options: {
				trackingIds: [`G-BDEPSSNLKS`],
			},
		},
		// {
		// 	resolve: `gatsby-plugin-google-analytics`,
		// 	options: {
		// 		trackingId: `UA-163347486-1`,
		// 	},
		// },
		{
			resolve: `gatsby-plugin-yandex-metrika`,
			options: {
				trackingId: '48229769',
				webvisor: true,
				trackHash: true,
			},
		},
		{
			resolve: `gatsby-plugin-manifest`,
			options: {
				name: `gatsby-starter-default`,
				short_name: `starter`,
				start_url: `/`,
				background_color: `#0079ff`,
				theme_color: `#ff1268`,
				display: `minimal-ui`,
				icon: `src/images/logo.png`, // This path is relative to the root of the site.
			},
		},
	],
};
